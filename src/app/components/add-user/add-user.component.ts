import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { MockuserService } from 'src/app/services/mockuser.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  user: User = {
    firstname: '',
    lastname: '',
    email: ''
  }
  submitted = false;

  constructor(private userService: MockuserService) { }

  ngOnInit(): void {
    console.log("add-user aangeroepen")
  }

  saveUser(): void {
    this.userService.create(this.user);
    this.submitted = true;
  }

  newUser(): void {
    this.submitted = false;
    this.user = {
      firstname: '',
      lastname: '',
      email: ''
    }
  }

}
