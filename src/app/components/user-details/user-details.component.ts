import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { MockuserService } from 'src/app/services/mockuser.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  currentUser: User = {
    firstname: '',
    lastname: '',
    email: ''
  }
  message = ''

  constructor(
    private userService: MockuserService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.message = ''
    this.getUser(this.route.snapshot.params['id'])
  }

  getUser(id: string): void {
    console.log('komen we hier?')
    this.currentUser = this.userService.get(id)
  }

  deleteUser(): void {
    console.log('yet to implement')
  }

  updateUser(): void {
    console.log('yet to implement')
  }

}
