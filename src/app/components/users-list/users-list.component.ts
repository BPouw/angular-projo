import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { MockuserService } from 'src/app/services/mockuser.service';


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  users?: User[];
  currentUser: User = {}
  currentIndex = -1;
  name = ''

  constructor(private userService: MockuserService) { }

  ngOnInit(): void {
    this.retrieveUsers()
  }

  retrieveUsers(): void {
    this.users = this.userService.getAll()
  }

  refreshList(): void {
    this.retrieveUsers();
    this.currentUser = {};
    this.currentIndex = -1;
  }

  setActiveUser(user: User, index: number): void {
    this.currentUser = user;
    this.currentIndex = index;
  }

  removeAllUsers(): void {
    console.log("nog te implementeren")
  }

  searchName(): void {
    console.log("nog te implementeren...")
  }

}
