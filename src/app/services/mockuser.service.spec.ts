import { TestBed } from '@angular/core/testing';

import { MockuserService } from './mockuser.service';

describe('MockuserService', () => {
  let service: MockuserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MockuserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
