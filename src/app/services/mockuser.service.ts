import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class MockuserService {

  private USERS: User[] = [
    {
        id: 1,
        firstname: 'Jane',
        lastname: 'Doe',
        email: 'janedoe@gmail.com'
    },
    {
        id: 2,
        firstname: 'John',
        lastname: 'Doe',
        email: 'johndoe@gmail.com'
    }
];

  constructor(private http: HttpClient) { }

  getAll(): User[] {
    return this.USERS;
  }

  create(user: User): void {
    this.USERS.push(user);
  }

  get(id: string): User {
    let usa = this.USERS.find(x => x.id == id)
    if (usa?.id == id) {
      return usa
    } else {
      return new User()
    }
  }


}
